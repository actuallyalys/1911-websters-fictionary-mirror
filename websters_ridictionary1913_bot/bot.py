"""bot.py -- Core code for the bot."""

import logging
import sys
import csv
import zipfile
import io
import pathlib

import yaml
from mastodon import Mastodon

import websters_ridictionary1913_bot.database as db


def create_connection_from_key(conn_config: dict) -> Mastodon:
    """Creates a connection using a configuration dictionary."""

    mastodon = Mastodon(**conn_config["instance"])

    return mastodon


def load_config() -> dict:
    """Load configuration."""

    with open("config/config.yaml") as f:
        loaded_config = yaml.safe_load(f.read())

    return loaded_config


def reverse_enumerate(iterator, start=None):
    """Same as enumerate, but counts backwards.

    If start is not provided, it uses len(iterator). If iterator doesn't
    implement __len__, raises a TypeError.
    """

    if start is None:
        try:
            start = len(iterator) - 1
        except TypeError as e:
            raise TypeError("start not provided and could not be calculated based on len." + str(e))

    for item in iterator:
        yield start, item

        start -= 1


def truncate_string(text: str, limit: int, truncation="…") -> str:

    if len(text) <= limit:
        return text

    # We have to find a stop point such that adding a truncation character
    # doesn't push us over the limit:
    start_point = limit - len(truncation)

    for i, char in reverse_enumerate(text[start_point:0]):

        if char in " \t\n":
            truncation_point = i
            break
    else:
        truncation_point = start_point

    return text[:truncation_point] + truncation


def print_help() -> None:
    print(f"""USAGE: {sys.argv[0]} COMMAND

    Commands:
    load   - load entries into dictionary
    post   - post a single status
    init   - creates the database
    status - show the
    help   - prints this help message""")


if __name__ == '__main__':
    # if False:
    config = load_config()
    client = create_connection_from_key(config)
    character_limit = config.get('character_limit', 500)
    database = config.get('database')

    command = sys.argv[1].lower()

    if command == 'load':
        filepath = pathlib.Path(sys.argv[2].strip())
        filename = filepath.name

        if filename.endswith(".zip"):
            print("uncompressing")
            with zipfile.ZipFile(filepath) as zf:
                compressed_file_name = filename.replace(".zip", "")
                with io.TextIOWrapper(zf.open(compressed_file_name, mode='r'), encoding="utf-8") as f:
                    lines = f.readlines()

        else:
            with open(filepath, 'r', encoding="utf8") as f:
                lines = f.readlines()


        reader = csv.reader(lines)
        next(reader)  # skip
        for raw, word, pos, definition, model_name, temperature in reader:
            db.add_definition(database, word, definition, pos, model_name, temperature)

    elif command == 'post':
        character_limit = config.get('character_limit', 500)
        try:
            definition_id, word, part_of_speech, definition_text, _ = db.get_definition(database)
            print(f"Posting definition for {word} {part_of_speech}")

            truncated_definition = truncate_string(definition_text,
                                                   (character_limit - 1 - 4 - len(word) - len(part_of_speech)))

            combined_definition = f"{word}, {part_of_speech}, {truncated_definition}"

            client.status_post(combined_definition, visibility="public")

            db.mark_definition_posted(database, definition_id, combined_definition)
        except ValueError as _e:
            print("Could not post definition.")
    elif command == 'init':
        db.create_database(database)
    elif command == 'status':
        post_count = db.count_posted(database)
        definition_count = db.count_definitions(database)

        print(f"Posts:\t\t{post_count:7d}\nDefinitions:\t{definition_count:7d}\n\n")

        print("{:27s}\t{:69s}".format("Date", "Post"))

        for date, post in db.get_last_posts(database):
            print(f"{date:27s}\t{post:69s}")

    elif command == 'help':
        print_help()
    elif command:
        print(f"Command {command} not found")
        print_help()
    else:
        print("Command required.")
        print_help()
