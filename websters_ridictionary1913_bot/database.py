import datetime
import sqlite3
import typing


def create_database(db_address) -> sqlite3.Connection:
    """Creates a new database."""

    conn = sqlite3.connect(db_address)

    conn.execute("""CREATE TABLE definitions
                (definition_id INTEGER PRIMARY KEY AUTOINCREMENT,
                --external_id INTEGER,
                word TEXT,
                definition_text TEXT,
                part_of_speech TEXT,
                model_name TEXT,
                temperature REAL,
                added TEXT,
                allowed INTEGER)""")

    conn.execute("""CREATE TABLE posts
                (post_id INTEGER PRIMARY KEY AUTOINCREMENT,
                definition_id INTEGER,
                posted TEXT, --date
                raw_post TEXT)""")

    conn.commit()

    return conn


def already_stored(db_address, word: str) -> bool:
    """Checks whether a definition with that ID already exists."""

    conn = sqlite3.connect(db_address)

    results = conn.execute("""SELECT id FROM definitions WHERE word = ?""",
                           (word,))

    return results.fetchone() is not None


def count_posted(db_address) -> int:
    """Counts the number of existing posts"""

    conn = sqlite3.connect(db_address)

    results = conn.execute("""SELECT count(*) FROM posts""")

    return results.fetchone()[0]


def count_definitions(db_address) -> int:
    """Counts the number of existing definitions"""
    conn = sqlite3.connect(db_address)

    results = conn.execute("""SELECT count(*) FROM definitions""")

    return results.fetchone()[0]


def get_last_posts(db_address) -> typing.List[typing.Tuple[str, str]]:
    """Return the latest."""
    conn = sqlite3.connect(db_address)

    results = conn.execute("""SELECT posted, raw_post FROM posts ORDER BY posted DESC LIMIT 10""")

    return results.fetchall()


def add_definition(db_address, word: str, definition_text: str, part_of_speech: str,
                   model_name: str, temperature: float) -> None:
    """Adds a definition to the database to be posted later."""

    conn = sqlite3.connect(db_address)
    now = datetime.datetime.now().isoformat()

    conn.execute("""INSERT INTO definitions
                 (word, definition_text, part_of_speech, model_name, temperature, added, allowed)
                 VALUES (:word, :definition_text, :part_of_speech, :model_name, :temperature, :added, 1)""",
                 {"word": word, "definition_text": definition_text,
                  "part_of_speech": part_of_speech, "model_name": model_name,
                  "temperature": temperature, "added": now})

    conn.commit()


def add_definition_if_not_exists(db_address, word: str, definition_text: str,
                                 part_of_speech: str, temperature: float) -> None:

    if not already_stored(db_address, word):
        add_definition(db_address, word, definition_text, part_of_speech, temperature)


# TODO Create a context manager version of this?
def get_definition(db_address) -> tuple:
    """Grabs an unposted, allowed definition.

    Does not mark the definition posted—do that with mark_definition_posted."""
    conn = sqlite3.connect(db_address)

    results = conn.execute("""SELECT definitions.definition_id, word, part_of_speech, definition_text, count(posts.post_id) AS count
                              FROM definitions
                              LEFT OUTER JOIN posts ON (definitions.definition_id = posts.definition_id)
                            GROUP BY definitions.definition_id, word, part_of_speech, definition_text
                            ORDER BY count ASC, RANDOM()""")

    if results:  # and results.rowcount > 0:
        definition_id, word, part_of_speech, definition_text, count = results.fetchone()

        return definition_id, word, part_of_speech, definition_text, count

    else:
        return tuple()


def mark_definition_posted(db_address, definition_id: int, raw_post: str) -> list:
    """Marks a definition as just having been posted."""
    conn = sqlite3.connect(db_address)
    now = datetime.datetime.now().isoformat()

    results = conn.execute(("""INSERT INTO posts (definition_id, posted, raw_post)
                                VALUES (:definition_id, :posted_date, :raw_post)
                                RETURNING post_id"""),
                           {"definition_id": definition_id, "posted_date": now,
                            "raw_post": raw_post})

    results_list = results.fetchall()

    conn.commit()
    return results_list
